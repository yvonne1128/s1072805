﻿using s1072805.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1072805.Controllers
{
    public class BMIController : Controller
    {   
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        { 
            if (ModelState.IsValid)
            {
                var m_height = data.height.Value / 100;
                var result = data.weight / (m_height * m_height);
                data.Result = result;
                var level = "" ;
                if (result <18.5)
                {
                    level = "體重過輕";
                }else if(result>=18.5 && result < 24)
                {
                    level = "正常範圍";
                }else if (result >= 24 && result < 27)
                {
                    level = "過重";
                }else if (result >= 27 && result < 30)
                {
                    level = "輕度肥胖";
                }else if (result >= 30 && result < 35)
                {
                    level = "中度肥胖";
                }else
                {
                    level = "重度肥胖";
                }
                data.Level = level ;
            }
            return View(data);
        }
    }
}